// Verto Maps
// Version: 1.0.1
// Author: Cameron Thomas

// Register handler for when the document is ready
document.addEventListener("DOMContentLoaded", vertoMapsInit);

// Object to store all maps
var vertoMaps = {};

// Array of available map themes
var vertoMapThemes = [];

// Create the AJAX request to retrieve the themes
themesRequest = new XMLHttpRequest();

function vertoMapsInit() {

    // Check if Maps has been loaded
    if (typeof google === "undefined" || typeof google.maps === "undefined") {

        // Maps not loaded
        console.log("Verto Maps: Google Maps has not been loaded. Please ensure you have loaded Google Maps on the page, with a valid API key, prior to loading Verto Maps.");

    }
    else {

        // Retrieve all <div> elements with data-map attribute set
        var mapContainers = document.querySelectorAll("div[data-map]");

        // Loop through all map containers
        mapContainers.forEach(function (mapItem) {

            // Get the map ID from the <div>
            var mapID = mapItem.getAttribute("data-map");

            // Retrieve the coordinates from the <div>
            var mapLocation = {
                lat: parseFloat(mapItem.getAttribute("data-latitude")),
                lng: parseFloat(mapItem.getAttribute("data-longitude"))
            };

            // Attempt to retrieve the zoom level from the <div>
            var mapZoom = parseInt(mapItem.getAttribute("data-zoom"));

            // Default to 18 if no zoom level is specified
            if (isNaN(mapZoom) || mapZoom === 0) {
                mapZoom = 18;
            }

            // Generate the map
            var map = new google.maps.Map(mapItem, {
                zoom: mapZoom,
                center: mapLocation,
            });

            // Check if map ID already exists
            if (typeof vertoMaps[mapID] !== "undefined") {
                console.log("Verto Maps: multiple maps exist with the ID of " + mapID + ". All map IDs must be unique.");
            }

            // Add the map to the global array
            vertoMaps[mapID] = map;

        });

        // Retrieve all theme selectors
        var mapSelects = document.querySelectorAll("select[data-map]");

        // Loop through all theme selectors
        mapSelects.forEach(function (themeItem) {

            // Add the event handlers
            themeItem.onchange = vmThemeSelected;

        });

        // Set event handler to handle the AJAX response
        themesRequest.onreadystatechange = vmLoadThemes;

        // Send the request
        themesRequest.open("GET", "/features/maps/GetMapThemes.ashx");
        themesRequest.send();

    }

}

// Handles the response to the AJAX request which retrieves available themes
function vmLoadThemes() {

    // Ensure the request is complete
    if (themesRequest.readyState === XMLHttpRequest.DONE) {

        // Ensure we have received a 200 OK response
        if (themesRequest.status === 200) {

            // Set the map themes object
            vertoMapThemes = JSON.parse(themesRequest.responseText);

            // Set themes for maps with data-map-theme attribute set
            vmApplyThemes();

            // Populate all theme <select> elements
            vmPopulateThemeSelects();


        } else {
            // No 200 response; something went wrong
            console.log("Verto Maps: failed to retrieve map themes. HTTP " + themesRequest.status);
        }

    }

}

// Populates all theme <select> elements with available themes
function vmPopulateThemeSelects() {

    // Retrieve <select> elements which are to be populated with themes
    var themeSelects = document.querySelectorAll("select[data-theme-populate=true]");

    // Loop through selects
    themeSelects.forEach(function (themeSelect) {

        // Clear existing options
        themeSelect.innerHTML = "";

        // Create the default option
        var defaultOption = document.createElement("option");

        // Set attributes for the default option
        defaultOption.value = "default";
        defaultOption.text = "Default";

        // Add the default option to the select
        themeSelect.appendChild(defaultOption);

        // Loop through all themes
        for (var key in vertoMapThemes) {

            // Create a new option element
            var newOption = document.createElement("option");

            // Set the option value
            newOption.value = key;

            // Create a friendly name and set the option text
            newOption.text = vmTitleCase(key.split("-").join(" "));

            // Add the new option to the select
            themeSelect.appendChild(newOption);

        }

    });

}

// Applies themes to maps with data-theme attribute set
function vmApplyThemes() {

    // Retrieve all <div> elements with data-map and data-map-theme attributes set
    var mapContainers = document.querySelectorAll("div[data-map][data-map-theme]");

    // Loop through maps
    mapContainers.forEach(function (vertoMap) {

        // Retrieve the map name
        var mapName = vertoMap.getAttribute("data-map");

        // Retrieve the theme name
        var themeName = vertoMap.getAttribute("data-map-theme");

        // Check there is a value
        if (typeof themeName !== "undefined" && themeName.trim() !== "") {

            // Check if the specified theme exists
            if (typeof vertoMapThemes[themeName] !== "undefined") {

                // Check if the map exists
                if (typeof vertoMaps[mapName] !== "undefined") {

                    // Set the theme
                    vmSetTheme(mapName, themeName);

                }
                else {

                    // Map does not exist
                    console.log("Verto Maps: map " + mapName + " does not exist.");

                }                

            }
            else {

                // Map does not exist
                console.log("Verto Maps: theme " + themeName + " does not exist.");

            }

        }
        else {

            // Theme name not set properly
            console.log("Verto Maps: data-map-theme added to " + mapName + " but no value has been set.");

        }

    });

}

// Converts a string to Title Case
function vmTitleCase(str) {
    return str.replace(/\w\S*/g, function (txt) { return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); });
}

// Handles the selection of a new theme in an associated <select> element
function vmThemeSelected(event) {

    // Retrieve the associated map ID
    var mapID = this.getAttribute("data-map");

    // Set the theme
    vmSetTheme(mapID, this.value)

}

// Sets the theme for the specified map
function vmSetTheme(map, theme) {

    // Check the specified map exists
    if (typeof vertoMaps[map] !== "undefined") {

        // Check if we're resetting to the default theme
        if (theme === "default") {

            // Set the theme
            vertoMaps[map].setOptions({
                styles: {}
            });

        }
        else {

            // Check the specified theme exists
            if (typeof vertoMapThemes[theme] !== "undefined") {

                // Set the theme
                vertoMaps[map].setOptions({
                    styles: vertoMapThemes[theme]
                });

            }
            else {

                // Theme does not exist
                console.log("Verto Maps: theme with ID of " + theme + " does not exist.");

            }

        }

    } else {

        // Map does not exist
        console.log("Verto Maps: map with ID of " + map + " does not exist.");

    }

}