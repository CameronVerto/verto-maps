### Adding Verto Maps to a Project ###

1. Download the latest release of Verto Maps and extract it.
2. [Add Google Maps to your page](https://developers.google.com/maps/documentation/javascript/), with a valid API key. Example: <script src="https://maps.googleapis.com/maps/api/js?key={YOUR_API_KEY_HERE}"></script>
3. Copy *verto.maps.js* (and/or *verto.maps.min.js*) to your website's *scripts/* directory and include it in your page, **beneath Google Maps**.
4. Create a directory in the root of your website called *features/* if it does not already exist, then create a sub-directory called *maps/*.
5. Copy *GetMapThemes.ashx* to the newly created *features/maps/* directory.

### Using Verto Maps ###

To use Verto Maps, add a *div* element to your page and then add a *data-map* attribute to your *div* with a unique ID for your map. Then add *data-latitude* and *data-longitude* attributes to your *div* to set the map location:

<div class="map" data-map="example-map" data-latitude="52.375041" data-longitude="-0.713666"></div>

Verto Maps includes no styling, so don't forget to set a width and height for your new *div* in your CSS. You may also use the *data-zoom* attribute to your *div* (must be a whole number) to set the zoom level of the map. The default value for this is 18.

### Using Map Themes/Styles ###

1. Create a sub-directory on your website under */features/maps/* called *themes/* if it does not already exist.
2. Go to [Snazzy Maps](https://snazzymaps.com/) (or your Google Maps style resource of choice) and find the theme that you would like to use.
3. On your chosen theme, copy the *JavaScript Style Array* and paste it into a new file.
4. Save the new file as *your-theme-name.vmt* in the *themes/* directory.
5. Add a *data-map-theme* attribute to your map's *div* element, and set the value to the name of the file you just created (without the file extension), e.g. *data-map-theme="your-theme-name"*.

### Map Theme Selectors ###

You can add *select* elements to your page which can be used to switch a map's theme. Simply add a *select* to your page, and then set the value of each *option* element to the name of an available theme. Just set the *data-map* attribute on your *select* to the ID of your map. Following the example above, you could have the following:

<label>Choose a map theme:</label>

<select data-map="example-map">
	<option value="default" selected>Default</option>
	<option value="your-theme-name">Custom Theme</option>
</select>

If you would like a *select* to be automatically populated with all available themes (every file in the *themes/* directory), just add *data-theme-populate="true"* to your *select* like so:

<select data-map="example-map" data-theme-populate="true">
	<!-- This element will automatically be populated with all available themes -->
</select>

### Accessing the Google Map Objects ###

All Google Map objects can be accessed via the *vertoMaps["yourMapID"]* object. You can interact with this like any other Google Map, i.e. add markers, manually change options, etc.

### Checking Available Themes ###

All available themes are stored in the *vertoMapThemes* object. Running *console.log(vertoMapThemes);* will provide you with a list of these themes.

### Manually Setting Theme via JavaScript ###

If you would like to change the theme at runtime, rather than directly accessing the Google Map object, you can simply call *vmSetTheme("yourMapID", "your-theme-name");*.