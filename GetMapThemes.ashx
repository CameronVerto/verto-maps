﻿<%@ WebHandler Language="C#" Class="GetMapThemes" %>

// Verto Maps
// Version: 1.0
// Author: Cameron Thomas

using System;
using System.IO;
using System.Web;

/// <summary>
/// Retrieves all available Google Maps styles and returns as a JSON array. Built for Verto Maps.
/// </summary>
public class GetMapThemes : IHttpHandler
{
    // Whether the user must be logged in to receive a response
    private bool requiresAuthentication = false;

    // The directory in which to search for themes
    private string themeDirectory = "~/features/maps/themes/";

    public void ProcessRequest( HttpContext context )
    {
        // Check if user is logged in (if they need to be)
        if( !requiresAuthentication || context.User.Identity.IsAuthenticated )
        {
            // Get the full path to the theme directory
            themeDirectory = context.Server.MapPath( themeDirectory );

            // Get all files in the theme directory
            string[] themeFiles = Directory.GetFiles( themeDirectory );

            // Prepare the beginning of the output JSON
            string output = "{";

            // Loop through all files
            foreach( string themeFile in themeFiles )
            {
                // Get FileInfo for theme file
                FileInfo themeFileInfo = new FileInfo( themeFile );

                // Ensure theme is a .vmt (Verto Map Theme) file
                if( themeFileInfo.Extension == ".vmt" )
                {
                    // Read the file
                    string themeData = File.ReadAllText( themeFile );

                    // Check the file is not empty
                    if( !String.IsNullOrWhiteSpace( themeData ) )
                    {
                        // Add the data to the JSON output
                        output += "\"" + themeFileInfo.Name.Replace(".vmt", "") + "\":" + themeData + ",";
                    }
                }
            }

            // Remove trailing comma from JSON output and close the object
            output = output.TrimEnd( Convert.ToChar( "," ) ) + "}";

            // We will be returning JSON
            context.Response.ContentType = "application/json";

            // Spit it out
            context.Response.Write( output );
        }

    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}